package com.example.secondlab;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    boolean isComplete;
    static int unknownNumber;
    static int tryCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Scanner input = new Scanner(System.in);
        unknownNumber = (int)Math.floor(Math.random() * 100);
    }

    public void checkNumber(View view){
        TextView textView = (TextView) findViewById(R.id.resultLabel);
        EditText inputNumberEditText = (EditText) findViewById(R.id.inputNumber);
        Button button = (Button) findViewById(R.id.checkButton);

        if(isComplete){
            tryCount = 0;
            isComplete = false;
            button.setText(getString(R.string.checkButtonText));
            textView.setText(getString(R.string.defaultResultText));
            return;
        }

        if(inputNumberEditText.getText().toString().length()==0) {
            textView.setText(getString(R.string.isEmptyInputNumber));
            return;
        }

        int userNumber = Integer.parseInt(inputNumberEditText.getText().toString());
        if(userNumber < 1 || userNumber > 100){
            textView.setText(getString(R.string.notRangeNumber));
            inputNumberEditText.setText("");
            return;
        }

        tryCount++;
        inputNumberEditText.setText("");

        if (userNumber > unknownNumber){
            textView.setText(String.format(getString(R.string.isMoreNumber), userNumber));
            System.out.println("My example log: user input -" + userNumber);
        }
        else if(userNumber < unknownNumber){
            textView.setText(String.format(getString(R.string.isLessNumber), userNumber));
            System.out.println("My example log: user input -" + userNumber);
        }
        else{
            isComplete = true;
            textView.setText(String.format(getString(R.string.isEquals), tryCount));
            button.setText(getString(R.string.repeatApp));
            unknownNumber = (int)Math.floor(Math.random() * 100);
        }
    }
}